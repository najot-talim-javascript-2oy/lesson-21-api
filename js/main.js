let usersRow = document.querySelector(".user-row");

function renderUsers({ name, username, email, phone, website, id }) {
  return ` 
  <div class="Cards">
    <div class="card" data-tilt="">
        <h2>Ismi: <span class="Ism">${name}</span></h2>
        <p>Username: <span class="Ism">${username}</span></p>
        <p class="Margin">Email: <a href="#">${email}</a></p>
        <p>Zipcode: <a href="#">${website}</a></p>
        <p>Phone: <a href="#">${phone}</a></p>
        <div class="links">
            <a href="https://www.instagram.com/samariddin_nurmamatov/"><img src="./images/facebook.png" alt=""></a>
            <a href="https://www.instagram.com/samariddin_nurmamatov/"><img src="./images/twitter.png" alt=""></a>
            <a href="https://www.instagram.com/samariddin_nurmamatov/"><img src="./images/telegram.png" alt=""></a>
        </div>
        <div class="buttonla">
          <a href="todo.html" onclick="saveId(${id})" class="btn">Go todo</a>
          <a href="post.html" onclick="saveId(${id})" class="btn">Go posts</a>
        </div>
        <a href="albom.html" onclick="saveId(${id})"  class="btn">Go album</a>
    </div>
  </div>
  `;
}
async function getUsers() {
  let res = await fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((json) => json);
  usersRow.innerHTML = "";
  res.forEach((user) => {
    usersRow.innerHTML += renderUsers(user);
  });
}
getUsers();

function saveId(id) {
  localStorage.setItem("userId", id);
}

document.addEventListener("contextmenu", (event) => event.preventDefault());

document.addEventListener("keydown", (event) => {
  if (
    (event.keyCode === 123,
    "I" || (event.ctrlKey && event.altKey && event.shiftKey))
  ) {
    event.preventDefault();
  }
});
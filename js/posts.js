const userId = localStorage.getItem("userId");
const postsRow = document.querySelector(".posts-row");
const page = userId;
const limit = 10;

function renderUsers({ title, body, id }) {
  return `
  <div class="card mb-3">
    <div class="card-body">
     <h6 class="card-title">${title}</h6>
     <p class="card-text">${body}</p>
     <a href="comment.html" onclick="savePostId(${id})" class="btn btn-primary">Go comments</a>
    </div>
  </div>
  `;
}
async function getUsers() {
  let res = await fetch(
    `https://jsonplaceholder.typicode.com/posts?_limit=${limit}&_page=${page}`
  )
    .then((response) => response.json())
    .then((json) => json);
  postsRow.innerHTML = "";
  res.forEach((user) => {
    postsRow.innerHTML += renderUsers(user);
  });
}
getUsers();
function savePostId(id) {
  localStorage.setItem("postId", id);
}

document.addEventListener("contextmenu", (event) => event.preventDefault());

document.addEventListener("keydown", (event) => {
  if (
    (event.keyCode === 123,
    "I" || (event.ctrlKey && event.altKey && event.shiftKey))
  ) {
    event.preventDefault();
  }
});
